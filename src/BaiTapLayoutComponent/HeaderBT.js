import React, { Component } from "react";

export default class HeaderBT extends Component {
  render() {
    return (
      <div className="bg-dark">
        <nav className="navbar navbar-expand-lg navbar-dark   ">
           <div className="container d-flex justify-content-between">
             <a className="navbar-brand " href="#">
              Start Boostrap
            </a>
          
          <div  id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item active">
                <a className="nav-link" href="#">
                  Home <span className="sr-only">(current)</span>
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  About
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Contact
                </a>
              </li>
            </ul>
          </div>
           </div>
        </nav>
      </div>
    );
  }
}
