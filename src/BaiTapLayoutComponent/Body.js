import React, { Component } from 'react'
import BannerBT from './BannerBT'
import Item from './Item'

export default class Body extends Component {
  render() {
    return (
      <div>
       <BannerBT />
       <Item/>
      </div>
    )
  }
}
