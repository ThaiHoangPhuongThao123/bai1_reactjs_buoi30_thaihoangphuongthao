import React, { Component } from 'react'

export default class BannerBT extends Component {
  render() {
    return (
      <div className='text-center bg-light p-5'>
        
          <div className="m-5 ">
         
          <h1 className='text-center display-5 fw-bold'>A warm welcome!</h1>
          <p className='font-p'>Bootstrap utility classes are used to create this jumbotron since the old component has been removed from the framework. Why create custom CSS when you can use utilities?</p>
        <button className='btn btn-primary btn-lg'>Call to action</button>
    
        
        </div>
      </div>
    )
  }
}
