import React, { Component } from 'react'
import HeaderBT from './HeaderBT'
import Body from './Body'
import FooterBT from './FooterBT'

export default class BaiTapThucHanhLayout extends Component {
  render() {
    return (
      <div >
        <HeaderBT/>
       <div className='container py-5'>
        <Body/>
       </div>
       <FooterBT/>
      </div>
    )
  }
}
