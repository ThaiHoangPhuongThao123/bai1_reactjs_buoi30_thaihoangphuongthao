import React, { Component } from "react";

export default class Item extends Component {
  render() {
    return (
      <div className="pt-4">
        <div className="row">
          <div className="col-lg-6 col-xl-4 mt-5">
            <div className="card bg-light border-0 h-100 ">
              <div className="card-body text-center bg-light px-5 pb-5 pt-0 item-body">
                <div className="item-icon bg-primary bg-gradient text-white  mb-4 ">
                  <i class="fa fa-images"></i>
                </div>

                <div className="item-text">
                    <h2 className="fw-bold font-p ">Fresh new layout</h2>
                <p className="mb-0">
                  With Bootstrap 5, we've created a fresh new layout for this
                  template!
                </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-xl-4 mt-5">
            <div className="card bg-light border-0 h-100 ">
              <div className="card-body text-center bg-light px-4 pb-4 pt-0 item-body">
                <div className="item-icon bg-primary bg-gradient text-white  mb-4 ">
                  <i class="fa fa-images"></i>
                </div>

                <div className="item-text">
                    <h2 className="fw-bold font-p ">Fresh new layout</h2>
                <p className="mb-0">
                  With Bootstrap 5, we've created a fresh new layout for this
                  template!
                </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-xl-4 mt-5">
            <div className="card bg-light border-0 h-100 ">
              <div className="card-body text-center bg-light px-4 pb-4 pt-0 item-body">
                <div className="item-icon bg-primary bg-gradient text-white  mb-4 ">
                  <i class="fa fa-images"></i>
                </div>

                <div className="item-text">
                    <h2 className="fw-bold font-p ">Fresh new layout</h2>
                <p className="mb-0">
                  With Bootstrap 5, we've created a fresh new layout for this
                  template!
                </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-xl-4 mt-5">
            <div className="card bg-light border-0 h-100 ">
              <div className="card-body text-center bg-light px-4 pb-4 pt-0 item-body">
                <div className="item-icon bg-primary bg-gradient text-white  mb-4 ">
                  <i class="fa fa-images"></i>
                </div>

                <div className="item-text">
                    <h2 className="fw-bold font-p ">Fresh new layout</h2>
                <p className="mb-0">
                  With Bootstrap 5, we've created a fresh new layout for this
                  template!
                </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-xl-4 mt-5">
            <div className="card bg-light border-0 h-100 ">
              <div className="card-body text-center bg-light px-4 pb-4 pt-0 item-body">
                <div className="item-icon bg-primary bg-gradient text-white  mb-4 ">
                  <i class="fa fa-images"></i>
                </div>

                <div className="item-text">
                    <h2 className="fw-bold font-p ">Fresh new layout</h2>
                <p className="mb-0">
                  With Bootstrap 5, we've created a fresh new layout for this
                  template!
                </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-xl-4 mt-5">
            <div className="card bg-light border-0 h-100 ">
              <div className="card-body text-center bg-light px-4 pb-4 pt-0 item-body">
                <div className="item-icon bg-primary bg-gradient text-white  mb-4 ">
                  <i class="fa fa-images"></i>
                </div>

                <div className="item-text">
                    <h2 className="fw-bold font-p ">Fresh new layout</h2>
                <p className="mb-0">
                  With Bootstrap 5, we've created a fresh new layout for this
                  template!
                </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
